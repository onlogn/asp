﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbApplication
{
    public class SqlActions
    {
        private readonly string _connectionStr;

        public SqlActions(string conStr)
        {
            _connectionStr = conStr;
        }

        public void InsertData(Student student)
        {
            string dayofBirth = student.BirthDateTime.HasValue ? "@dayOfBirth" : "NULL";
            string query = $"INSERT INTO Students(StudentName,BirthDate) VALUES(@name,{dayofBirth})";
            using (SqlConnection con = new SqlConnection(_connectionStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(query, con);

                SqlParameter pName = new SqlParameter("@name", student.StudentName);
                cmd.Parameters.Add(pName);

                if (student.BirthDateTime != null)
                {
                    SqlParameter pDate = new SqlParameter("@dayOfBirth", student.BirthDateTime);
                    cmd.Parameters.Add(pDate);
                }

                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateData(Student student)
        {
            string query = "UPDATE Students SET StudentName=@name, BirthDate=@dayOfBirth WHERE Id=@id";
            using (SqlConnection con = new SqlConnection(_connectionStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(query, con);

                SqlParameter pName = new SqlParameter("@name", student.StudentName);
                cmd.Parameters.Add(pName);

                SqlParameter pDate = new SqlParameter("@dayOfBirth", student.BirthDateTime);
                cmd.Parameters.Add(pDate);

                SqlParameter pId = new SqlParameter("@id", student.Id);
                cmd.Parameters.Add(pId);

                cmd.ExecuteNonQuery();
            }
        }

        public int DeleteData(int id)
        {
            string query = "DELETE FROM Students WHERE Id=@id";
            using (SqlConnection con = new SqlConnection(_connectionStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(query, con);

                SqlParameter pId = new SqlParameter("@id", id);
                cmd.Parameters.Add(pId);

                return cmd.ExecuteNonQuery();
            }
        }

        public List<Student> SelectData(int id)
        {
            List<Student> students = new List<Student>();

            string whereCondition = id <= 0 ? "" : " WHERE Id=@id";

            string query = "SELECT Id, StudentName, BirthDate FROM Students" + whereCondition;

            using (SqlConnection con = new SqlConnection(_connectionStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(query, con);

                if (!String.IsNullOrEmpty(whereCondition))
                {
                    SqlParameter pId = new SqlParameter("@id", id);
                    cmd.Parameters.Add(pId);
                }

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        students.Add(new Student()
                        {
                            Id = reader.GetInt32(0), StudentName = reader.GetString(1),
                            BirthDateTime = reader.IsDBNull(2) ? (DateTime?)null : reader.GetDateTime(2)
                        });
                    }
                }

                reader.Close();
            }

            return students;
        }
    }
}
