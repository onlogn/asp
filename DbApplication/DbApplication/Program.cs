﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlActions sql = new SqlActions(ConfigurationManager.ConnectionStrings["StudentDbCon"].ConnectionString);

            while (true)
            {
                Console.WriteLine("Choose action:");
                Console.WriteLine("1) Select");
                Console.WriteLine("2) Insert");
                Console.WriteLine("3) Update");
                Console.WriteLine("4) Delete");
                Console.WriteLine("5) Exit");

                var key = Console.ReadKey();
                Console.WriteLine();

                try
                {

                    switch (key.KeyChar)
                    {
                        case '1':
                            Console.WriteLine("Select data");
                            Console.Write("id = (if u want select all, press ENTER)");
                            int.TryParse(Console.ReadLine(), out int id);
                            List<Student> students = sql.SelectData(id);
                            Console.WriteLine("Students table:");
                            foreach (Student student in students)
                            {
                                Console.WriteLine(student);
                            }

                            break;
                        case '2':
                            Console.WriteLine("Insert data");
                            Console.Write("StudentName: ");
                            string studentName = Console.ReadLine().Trim();
                            while (string.IsNullOrEmpty(studentName))
                            {
                                Console.Write("You must enter studentName: ");
                                studentName = Console.ReadLine().Trim();
                            }
                            Console.Write("Date of Birth (Format: dd.MM.yyyy): ");

                            DateTime? date = GetDate();

                            sql.InsertData(new Student() {StudentName = studentName, BirthDateTime = date});

                            break;

                        case '3':
                            Console.WriteLine("Update data");
                            Console.Write("id = ");

                            int studentId = int.Parse(Console.ReadLine().Trim());
                            Student currentStudent = sql.SelectData(studentId).FirstOrDefault();
                            if (currentStudent == null) throw new Exception("Incorrect id");

                            Console.WriteLine("Input new name: (if u don't want update studentName, press ENTER");
                            string name = Console.ReadLine().Trim();
                            currentStudent.StudentName = string.IsNullOrEmpty(name) ? currentStudent.StudentName : name;

                            Console.WriteLine("Input new DayOdBirth (Format: dd.MM.yyyy): (if u don't want update DateOfBirth, press ENTER");

                            DateTime? dayOfBirth = GetDate();
                            currentStudent.BirthDateTime = dayOfBirth ?? currentStudent.BirthDateTime;

                            sql.UpdateData(currentStudent);
                            break;

                        case '4':
                            Console.WriteLine("Delete data");
                            Console.Write("id = ");
                            int currentId = int.Parse(Console.ReadLine().Trim());

                            sql.DeleteData(currentId);

                            break;

                        case '5':
                            return;

                        default:
                            Console.WriteLine("Unknown Command");
                            break;
                    }
                }

                catch (Exception e)
                {
                    Console.WriteLine($"Exception: {e.Message}");
                }

            }
        }

        private static DateTime? GetDate()
        {
            DateTime? date;
            try
            {
                date = DateTime.ParseExact(Console.ReadLine().Trim(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
            }
            catch (FormatException e)
            {
                Console.Write($"Bad format, so it's NULL. {e.Message}");
                date = null;
            }

            return date;
        } 
    }
}
