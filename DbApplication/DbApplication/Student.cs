﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbApplication
{
    public class Student
    {
        public int Id { get; set; }
        public string StudentName { get; set; }
        public DateTime? BirthDateTime { get; set; }

        public override string ToString()
        {
            string nullStr = "NULL";
            return $"{Id}  {StudentName} {BirthDateTime?.ToString() ?? nullStr}";
        }
    }
}
