﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewsHomeWork.Models;

namespace ViewsHomeWork.Controllers
{
    public class HomeController : Controller
    {
        
        [HttpGet]
        public ActionResult View1()
        {
            ViewBag.Message = "Hello world!";
            return View();
        }

        [HttpGet]
        public ActionResult View2()
        {
            User u = new User()
            {
                UserName = "dsgfsdfhgstf",
                    Email = "dasfds"
            };
            return View(u);
        }

        [HttpGet]
        public ActionResult View3()
        {
            List<User> users = new List<User>()
            {
                new User() {UserName = "user 1", Email = "mail 1"},
                new User() {UserName = "user 2", Email = "mail 2"}
            };

            return View(users);
        }

        [HttpGet]
        public ActionResult View4()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetView4(User user)
        {
            return PartialView(user);
        }
       
    }
}