﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewsHomeWork.Models
{
    public class User
    {
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}