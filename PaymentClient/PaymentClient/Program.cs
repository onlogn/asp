﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PaymentClient.ServiceReference1;

namespace PaymentClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var sc = new ServiceReference1.WebService1SoapClient();

            int ven = 1;
            int acc = 1;
            int sum = 50;

            OperationResult op = sc.ValidatePayment(ven, acc, sum);
            Console.WriteLine(op.ErrorCode != 0
                ? $"ERROR - {op.ErrorCode} - {op.ErrorMessage} - {op.Note}"
                : sc.MakePayment(ven, acc, sum, Guid.NewGuid()));
        }
    }
}
