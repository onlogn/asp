﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cars.Models
{
    public class Ad
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Input Value")]
        [Display(Name="Year")]
        [Range(0,3000, ErrorMessage = "Year mut be in range (0-3000)")]
        public int Year { get; set; }

        [Required(ErrorMessage = "Input Value")]
        [Display(Name = "Engine Volume")]
        [Range(0, 30, ErrorMessage = "Volume mut be in range (0-30)")]
        public double EngineVolume { get; set; }

        [Required(ErrorMessage = "Input Value")]
        [Range(typeof(decimal), "0.00", "1000000000.99")]
        public decimal Cost { get; set; }

        [Required(ErrorMessage = "Input Value")]
        public int Mileage { get; set; }

        [Required(ErrorMessage = "Input Value")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public string VendorText { get; set; }

        [Required(ErrorMessage = "Input Value")]
        public int ModelId { get; set; }
        public Model Model { get; set; }

        [Required(ErrorMessage = "Input Value")]
        public int GearBoxId { get; set; }
        public GearBox GearBox { get; set; }

        [Required(ErrorMessage = "Input Value")]
        public int ColorId { get; set; }
        public Color Color { get; set; }

        [Required(ErrorMessage = "Input Value")]
        public int FuelTypeId { get; set; }
        public FuelType FuelType { get; set; }

        [Required(ErrorMessage = "Input Value")]
        public int VendorId { get; set; }
        public Vendor Vendor { get; set; }
    }
}