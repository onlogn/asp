﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cars.Models
{
    public class FuelType
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Input data")]
        [Display(Name = "Fuel Type Name")]
        [RegularExpression(@"^[A-Za-zА-Яа-я. ]+$", ErrorMessage = "Incorrect format")]
        public string TypeName { get; set; }

        public List<Ad> Ads { get; set; }

    }
}