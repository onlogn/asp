﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cars.Models
{
    public class Vendor
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Input data")]
        [Display(Name = "Vendor Name")]
        [RegularExpression(@"^[A-Za-zА-Яа-я. ]+$", ErrorMessage = "Incorrect format")]
        public string Name { get; set; }

        [Display(Name = "Vendor Phone")]
        [RegularExpression(@"^[0-9-. ]+$", ErrorMessage = "Incorrect format")]
        public string Phone { get; set; }

        public List<Ad> Ads { get; set; }

    }
}