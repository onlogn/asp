﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cars.Models
{
    public class Model
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Input data")]
        [Display(Name = "Model Name")]
        [RegularExpression(@"^[A-Za-zА-Яа-я.0-9 ]+$", ErrorMessage = "Incorrect format")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Input data")]
        public int BrandId { get; set; }
        public Brand Brand { get; set; }


        public List<Ad> Ads { get; set; }
    }
}