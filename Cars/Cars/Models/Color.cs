﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cars.Models
{
    public class Color
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Input data")]
        [Display(Name = "Color Name")]
        [RegularExpression(@"^[A-Za-zА-Яа-я. ]+$", ErrorMessage = "Incorrect format")]
        public string Name { get; set; }

        public List<Ad> Ads { get; set; }

    }
}