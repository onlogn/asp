﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Cars.Models
{
    public class CarContext:DbContext
    {
        public CarContext():base("Cars")
        {
            
        }

        public DbSet<Ad> Ads { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<FuelType> FuelTypes { get; set; }
        public DbSet<GearBox> GearBoxes { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Vendor> Vendors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model>()
                .HasRequired(k => k.Brand)
                .WithMany(k => k.Models)
                .HasForeignKey(k => k.BrandId);

            modelBuilder.Entity<Ad>()
                .HasRequired(a => a.Model)
                .WithMany(m => m.Ads)
                .HasForeignKey(a => a.ModelId);

            modelBuilder.Entity<Ad>()
                .HasRequired(k => k.GearBox)
                .WithMany(k => k.Ads)
                .HasForeignKey(k => k.GearBoxId);

            modelBuilder.Entity<Ad>()
                .HasRequired(k => k.Color)
                .WithMany(k => k.Ads)
                .HasForeignKey(k => k.ColorId);

            modelBuilder.Entity<Ad>()
                .HasRequired(k => k.FuelType)
                .WithMany(k => k.Ads)
                .HasForeignKey(k => k.FuelTypeId);

            modelBuilder.Entity<Ad>()
                .HasRequired(k => k.Vendor)
                .WithMany(k => k.Ads)
                .HasForeignKey(k => k.VendorId);

        }
    }
}