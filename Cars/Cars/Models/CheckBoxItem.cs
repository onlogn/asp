﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace Cars.Models
{
    public class CheckBoxItem
    {
        public int CheckBoxId { get; set; }
        public string ItemName { get; set; }
        public bool IsChecked { get; set; }

    }
}