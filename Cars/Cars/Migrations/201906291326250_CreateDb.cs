﻿namespace Cars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ads",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        EngineVolume = c.Double(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Mileage = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        VendorText = c.String(),
                        ModelId = c.Int(nullable: false),
                        GearBoxId = c.Int(nullable: false),
                        ColorId = c.Int(nullable: false),
                        FuelTypeId = c.Int(nullable: false),
                        VendorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Colors", t => t.ColorId, cascadeDelete: true)
                .ForeignKey("dbo.FuelTypes", t => t.FuelTypeId, cascadeDelete: true)
                .ForeignKey("dbo.GearBoxes", t => t.GearBoxId, cascadeDelete: true)
                .ForeignKey("dbo.Models", t => t.ModelId, cascadeDelete: true)
                .ForeignKey("dbo.Vendors", t => t.VendorId, cascadeDelete: true)
                .Index(t => t.ModelId)
                .Index(t => t.GearBoxId)
                .Index(t => t.ColorId)
                .Index(t => t.FuelTypeId)
                .Index(t => t.VendorId);
            
            CreateTable(
                "dbo.Colors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FuelTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GearBoxes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BoxType = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Models",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        BrandId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brands", t => t.BrandId, cascadeDelete: true)
                .Index(t => t.BrandId);
            
            CreateTable(
                "dbo.Brands",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Vendors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Phone = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ads", "VendorId", "dbo.Vendors");
            DropForeignKey("dbo.Ads", "ModelId", "dbo.Models");
            DropForeignKey("dbo.Models", "BrandId", "dbo.Brands");
            DropForeignKey("dbo.Ads", "GearBoxId", "dbo.GearBoxes");
            DropForeignKey("dbo.Ads", "FuelTypeId", "dbo.FuelTypes");
            DropForeignKey("dbo.Ads", "ColorId", "dbo.Colors");
            DropIndex("dbo.Models", new[] { "BrandId" });
            DropIndex("dbo.Ads", new[] { "VendorId" });
            DropIndex("dbo.Ads", new[] { "FuelTypeId" });
            DropIndex("dbo.Ads", new[] { "ColorId" });
            DropIndex("dbo.Ads", new[] { "GearBoxId" });
            DropIndex("dbo.Ads", new[] { "ModelId" });
            DropTable("dbo.Vendors");
            DropTable("dbo.Brands");
            DropTable("dbo.Models");
            DropTable("dbo.GearBoxes");
            DropTable("dbo.FuelTypes");
            DropTable("dbo.Colors");
            DropTable("dbo.Ads");
        }
    }
}
