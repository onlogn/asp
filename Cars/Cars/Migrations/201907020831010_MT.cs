﻿namespace Cars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MT : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Colors", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.FuelTypes", "TypeName", c => c.String(nullable: false));
            AlterColumn("dbo.GearBoxes", "BoxType", c => c.String(nullable: false));
            AlterColumn("dbo.Models", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Vendors", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Vendors", "Name", c => c.String());
            AlterColumn("dbo.Models", "Name", c => c.String());
            AlterColumn("dbo.GearBoxes", "BoxType", c => c.String());
            AlterColumn("dbo.FuelTypes", "TypeName", c => c.String());
            AlterColumn("dbo.Colors", "Name", c => c.String());
        }
    }
}
