﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cars.Models;
using Cars.ViewModels;

namespace Cars.Controllers
{
    public class AdController : Controller
    {
        CarContext db = new CarContext();
        [HttpGet]
        public async Task<ActionResult> Index(CombineViewModel cm)
        {
            var gBoxes = await db.GearBoxes.ToListAsync();
            var fTypes = await db.FuelTypes.ToListAsync();

            cm = cm ?? new CombineViewModel();

            cm.AdViewModel = new AdViewModel();
            cm.AdViewModel.ColorSelectList = new SelectList(await db.Colors.ToListAsync(),
                nameof(Color.Id), nameof(Color.Name));
            cm.AdViewModel.FuelTypeSelectList = new SelectList(fTypes,
                nameof(FuelType.Id), nameof(FuelType.TypeName));
            cm.AdViewModel.GearBoxSelectList = new SelectList(gBoxes,
                nameof(GearBox.Id), nameof(GearBox.BoxType));

            cm.AdViewModel.BrandSelectList = new SelectList(await db.Brands.ToListAsync(),
                nameof(Model.Id), nameof(Model.Name));

            cm.AdViewModel.VendorSelectList = new SelectList(await db.Vendors.ToListAsync(),
                nameof(Vendor.Id), nameof(Vendor.Name));

            if (cm.FilterViewModel == null)
            {
                cm.FilterViewModel = new FilterViewModel();

                cm.FilterViewModel.GearBoxCkBoxItems = gBoxes.Select(c => new CheckBoxItem()
                {
                    CheckBoxId = c.Id,
                    ItemName = c.BoxType,
                    IsChecked = false
                }).ToList();

                cm.FilterViewModel.FuelTypeCkBoxItems = fTypes.Select(c => new CheckBoxItem()
                {
                    CheckBoxId = c.Id,
                    ItemName = c.TypeName,
                    IsChecked = false
                }).ToList();

                cm.Ad = await db.Ads
                    .Include(c => c.Model.Brand)
                    .Include(c => c.Color)
                    .Include(c => c.FuelType)
                    .Include(c => c.GearBox)
                    .Include(c => c.Vendor)
                    .ToListAsync();
                return View(cm);
            }
           

            var data = db.Ads
                .Include(c => c.Model.Brand)
                .Include(c => c.Color)
                .Include(c => c.FuelType)
                .Include(c => c.GearBox)
                .Include(c => c.Vendor);

            if (cm.FilterViewModel.ColorId != null)
            {
                data = data.Where(c => c.ColorId == cm.FilterViewModel.ColorId);
            }

            if (cm.FilterViewModel.BrandId != null)
            {
                data = data.Where(c => c.Model.BrandId == cm.FilterViewModel.BrandId);
            }

            if (cm.FilterViewModel.ModelId != null)
            {
                data = data.Where(c => c.ModelId == cm.FilterViewModel.ModelId);
            }

            if (cm.FilterViewModel.PriceFrom > 0)
            {
                data = data.Where(c => c.Cost > cm.FilterViewModel.PriceFrom);
            }

            if (cm.FilterViewModel.PriceTo > 0)
            {
                data = data.Where(c => c.Cost < cm.FilterViewModel.PriceTo);
            }

            if (cm.FilterViewModel.YearFrom > 0)
            {
                data = data.Where(c => c.Cost >= cm.FilterViewModel.YearFrom);
            }

            if (cm.FilterViewModel.YearTo > 0)
            {
                data = data.Where(c => c.Cost <= cm.FilterViewModel.YearTo);
            }

            if (cm.FilterViewModel.GearBoxCkBoxItems.Count(c => c.IsChecked) > 0)
            {
                List<int> checkedItems = cm.FilterViewModel.GearBoxCkBoxItems.Where(c => c.IsChecked).Select(c => c.CheckBoxId).ToList();
                //where gearboxId IN checkedItems
                data = data.Where(c => checkedItems.Contains(c.GearBoxId));

                cm.FilterViewModel.GearBoxCkBoxItems = gBoxes.Select(c => new CheckBoxItem()
                {
                    CheckBoxId = c.Id,
                    ItemName = c.BoxType,
                    IsChecked = checkedItems.Contains(c.Id)
                }).ToList();

            }
            else
            {
                cm.FilterViewModel.GearBoxCkBoxItems = gBoxes.Select(c => new CheckBoxItem()
                {
                    CheckBoxId = c.Id,
                    ItemName = c.BoxType,
                    IsChecked = false
                }).ToList();
            }

            if (cm.FilterViewModel.FuelTypeCkBoxItems.Count(c => c.IsChecked) > 0)
            {
                List<int> checkedItems = cm.FilterViewModel.FuelTypeCkBoxItems.Where(c => c.IsChecked)
                    .Select(c => c.CheckBoxId).ToList();
                data = data.Where(c => checkedItems.Contains(c.FuelTypeId));

                cm.FilterViewModel.FuelTypeCkBoxItems = fTypes.Select(c => new CheckBoxItem()
                {
                    CheckBoxId = c.Id,
                    ItemName = c.TypeName,
                    IsChecked = checkedItems.Contains(c.Id)
                }).ToList();
            }
            else
            {
                cm.FilterViewModel.FuelTypeCkBoxItems = fTypes.Select(c => new CheckBoxItem()
                {
                    CheckBoxId = c.Id,
                    ItemName = c.TypeName,
                    IsChecked = false
                }).ToList();
            }

            cm.Ad = await data.ToListAsync(); 

            return View(cm);
            
            

            
        }

        [HttpGet]
        public async Task<ActionResult> Create()
        {
            AdViewModel ad = new AdViewModel();
            ad.ColorSelectList = new SelectList(await db.Colors.ToListAsync(),
                nameof(Color.Id),nameof(Color.Name));
            ad.FuelTypeSelectList = new SelectList(await db.FuelTypes.ToListAsync(),
                nameof(FuelType.Id), nameof(FuelType.TypeName));
            ad.GearBoxSelectList = new SelectList(await db.GearBoxes.ToListAsync(),
                nameof(GearBox.Id), nameof(GearBox.BoxType));

            ad.BrandSelectList = new SelectList(await db.Brands.ToListAsync(),
                nameof(Model.Id), nameof(Model.Name));

            ad.VendorSelectList = new SelectList(await db.Vendors.ToListAsync(),
                nameof(Vendor.Id), nameof(Vendor.Name));
            return View(ad);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AdViewModel adVm)
        {
            if (!ModelState.IsValid) return HttpNotFound();
            db.Ads.Add(adVm.Ad);
            await db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            AdViewModel ad = new AdViewModel();

            ad.Ad = await db.Ads.FirstOrDefaultAsync(c => c.Id == id);
            if (ad.Ad == null) return HttpNotFound();

            ad.ColorSelectList = new SelectList(await db.Colors.ToListAsync(),
                nameof(Color.Id), nameof(Color.Name));
            ad.FuelTypeSelectList = new SelectList(await db.FuelTypes.ToListAsync(),
                nameof(FuelType.Id), nameof(FuelType.TypeName));
            ad.GearBoxSelectList = new SelectList(await db.GearBoxes.ToListAsync(),
                nameof(GearBox.Id), nameof(GearBox.BoxType));

            ad.BrandSelectList = new SelectList(await db.Brands.ToListAsync(),
                nameof(Model.Id), nameof(Model.Name));

            ad.VendorSelectList = new SelectList(await db.Vendors.ToListAsync(),
                nameof(Vendor.Id), nameof(Vendor.Name));

            ad.ModelSelectList = new SelectList(await db.Models.ToListAsync(),
                nameof(Model.Id), nameof(Model.Name));

            return View(ad);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(AdViewModel adwm)
        {
            if (!ModelState.IsValid) return HttpNotFound();

            db.Entry(adwm.Ad).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> GetModels(int brandId)
        {
            ModelViewModel mv = new ModelViewModel();
            mv.ModelSelectList = new SelectList(await db.Models
                .Where(b => b.BrandId == brandId).ToListAsync(),
                nameof(Model.Id), nameof(Model.Name));
            return PartialView(mv);
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}