﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cars.Models;

namespace Cars.Controllers
{
    public class ColorController : Controller
    {
        CarContext db = new CarContext();
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            return View(await db.Colors.ToListAsync());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Color color)
        {
            if (!ModelState.IsValid) return HttpNotFound();
            db.Colors.Add(color);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            var color = await db.Colors.FirstOrDefaultAsync(v => v.Id == id);
            if (color == null) return HttpNotFound();
            return View(color);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Color color)
        {
            if (!ModelState.IsValid) return HttpNotFound();

            db.Entry(color).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            var color = await db.Colors.FirstOrDefaultAsync(v => v.Id == id);
            if (color == null) return HttpNotFound();
            db.Colors.Remove(color);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}