﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cars.Models;

namespace Cars.Controllers
{
    public class FuelTypeController : Controller
    {
        CarContext db = new CarContext();
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            return View(await db.FuelTypes.ToListAsync());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FuelType FuelType)
        {
            if (!ModelState.IsValid) return HttpNotFound();
            db.FuelTypes.Add(FuelType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            var FuelType = await db.FuelTypes.FirstOrDefaultAsync(v => v.Id == id);
            if (FuelType == null) return HttpNotFound();
            return View(FuelType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(FuelType FuelType)
        {
            if (!ModelState.IsValid) return HttpNotFound();

            db.Entry(FuelType).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            var FuelType = await db.FuelTypes.FirstOrDefaultAsync(v => v.Id == id);
            if (FuelType == null) return HttpNotFound();
            db.FuelTypes.Remove(FuelType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}