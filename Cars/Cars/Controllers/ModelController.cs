﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cars.Models;
using Cars.ViewModels;

namespace Cars.Controllers
{
    public class ModelController : Controller
    {
        CarContext db = new CarContext();
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            return View(await db.Models.Include(c => c.Brand).ToListAsync());
        }

        [HttpGet]
        public async Task<ActionResult> Create()
        {
            BrandModelViewModel bm = new BrandModelViewModel();
            bm.BrandsSelectList = new SelectList(await db.Brands.ToListAsync(),
                nameof(Brand.Id), nameof(Brand.Name));
            return View(bm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(BrandModelViewModel bm)
        {
            Model model = bm.Models;
            db.Models.Add(model);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            var currentModel = await db.Models.FirstOrDefaultAsync(c => c.Id == id);
            if (currentModel == null) return HttpNotFound();
            
            BrandModelViewModel bm = new BrandModelViewModel();
            bm.BrandsSelectList = new SelectList(await db.Brands.ToListAsync(),
                nameof(Brand.Id), nameof(Brand.Name));
            bm.Models = currentModel;
            return View(bm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(BrandModelViewModel bm)
        {
            if (!ModelState.IsValid) return HttpNotFound();

            db.Entry(bm.Models).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            var model = await db.Models.FirstOrDefaultAsync(v => v.Id == id);
            if (model == null) return HttpNotFound();
            db.Models.Remove(model);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}