﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cars.Models;

namespace Cars.Controllers
{
    public class BrandController : Controller
    {
        CarContext db = new CarContext();
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            return View(await db.Brands.ToListAsync());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Brand Brand)
        {
            if (!ModelState.IsValid) return HttpNotFound();
            db.Brands.Add(Brand);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            var Brand = await db.Brands.FirstOrDefaultAsync(v => v.Id == id);
            if (Brand == null) return HttpNotFound();
            return View(Brand);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Brand Brand)
        {
            if (!ModelState.IsValid) return HttpNotFound();

            db.Entry(Brand).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            var Brand = await db.Brands.FirstOrDefaultAsync(v => v.Id == id);
            if (Brand == null) return HttpNotFound();
            db.Brands.Remove(Brand);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}