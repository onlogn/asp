﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cars.Models;

namespace Cars.Controllers
{
    public class GearBoxController : Controller
    {
        CarContext db = new CarContext();
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            return View(await db.GearBoxes.ToListAsync());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(GearBox GearBox)
        {
            if (!ModelState.IsValid) return HttpNotFound();
            db.GearBoxes.Add(GearBox);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            var GearBox = await db.GearBoxes.FirstOrDefaultAsync(v => v.Id == id);
            if (GearBox == null) return HttpNotFound();
            return View(GearBox);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(GearBox GearBox)
        {
            if (!ModelState.IsValid) return HttpNotFound();

            db.Entry(GearBox).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            var GearBox = await db.GearBoxes.FirstOrDefaultAsync(v => v.Id == id);
            if (GearBox == null) return HttpNotFound();
            db.GearBoxes.Remove(GearBox);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}