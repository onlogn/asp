﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Cars.Models;

namespace Cars.ViewModels
{
    public class FilterViewModel
    {
        public int? ColorId { get; set; }
        public int? BrandId { get; set; }
        public int? ModelId { get; set; }


        public int PriceFrom { get; set; }
        public int PriceTo { get; set; }


        [Range(0, 3000, ErrorMessage = "Out of range 0 - 3000")]
        public int YearFrom { get; set; }

        [Range(0, 3000, ErrorMessage = "Out of range 0 - 3000")]
        public int YearTo { get; set; }

        public List<CheckBoxItem> FuelTypeCkBoxItems { get; set; }

        public List<CheckBoxItem> GearBoxCkBoxItems { get; set; }

        
    }
}