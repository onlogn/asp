﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cars.Models;

namespace Cars.ViewModels
{
    public class BrandModelViewModel
    {
        public SelectList BrandsSelectList { get; set; }
        public Model Models { get; set; }
    }
}