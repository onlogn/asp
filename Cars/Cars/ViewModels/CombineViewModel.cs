﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cars.Models;

namespace Cars.ViewModels
{
    public class CombineViewModel
    {
        public AdViewModel AdViewModel { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public List<Ad> Ad { get; set; }
    }
}