﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cars.Models;

namespace Cars.ViewModels
{
    public class ModelViewModel
    {
        public Ad Ad { get; set; }
        public SelectList ModelSelectList { get; set; }
    }
}