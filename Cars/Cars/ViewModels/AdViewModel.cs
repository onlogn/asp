﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cars.Models;

namespace Cars.ViewModels
{
    public class AdViewModel
    {
        public Ad Ad { get; set; }
        public SelectList BrandSelectList { get; set; }
        public SelectList GearBoxSelectList { get; set; }
        public SelectList ColorSelectList { get; set; }
        public SelectList FuelTypeSelectList { get; set; }
        public SelectList VendorSelectList { get; set; }

        public SelectList ModelSelectList { get; set; }



    }
}