﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaymentService
{
    public class Account
    {
        public int AccountId { get; set; }
        public int Balance { get; set; }

        public int VendorId { get; set; }
        public Vendor Vendor { get; set; }
        public List<Payment> Payments { get; set; }
    }
}