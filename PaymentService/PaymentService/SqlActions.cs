﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using NLog;

namespace PaymentService
{
    public static class SqlActions
    {
        private static object _locker = new object();
        private static Logger _log = LogManager.GetCurrentClassLogger();

        public static Dictionary<int, List<int>> InitializeDictionary()
        {

            try
            {
                lock (_locker)
                {
                    Dictionary<int, List<int>> res;
                    using (PaymentContext pc = new PaymentContext())
                    {
                        res = pc.Accounts.GroupBy(v => v.Vendor.VendorId)
                            .ToDictionary(v => v.Key, v => v.Select(a => a.AccountId).ToList());

                    }

                    return res;
                }
            }
            catch (Exception e)
            {
                _log.Error(e, "Can't fill dictonary");
                throw;
            }
            
        }

        public static bool MakePaymentTran(int vendorId, int accountId, int sum, Guid paymentId)
        {

            using (PaymentContext paymentContext = new PaymentContext())
            {
                using (DbContextTransaction tran = paymentContext.Database.BeginTransaction())
                {
                    try
                    {
                        paymentContext.Payments.Add(new Payment()
                        {
                            VendorId = vendorId,
                            AccountId = accountId,
                            PaymentGuid = paymentId,
                            Sum = sum
                        });
                        paymentContext.SaveChanges();

                        Account currentAccount = paymentContext.Accounts.FirstOrDefault(a => a.AccountId == accountId);
                        if(currentAccount == null) throw new Exception();
                        currentAccount.Balance += sum;
                        paymentContext.SaveChanges();

                        tran.Commit();
                    }
                    catch (Exception e)
                    {
                        tran.Rollback();
                        _log.Error(e,"Transaction fail");
                        return false;
                    }
                }
            }

            return true;
        }

        public static bool GuidNotExists(Guid currentGuid)
        {
            try
            {
                using (PaymentContext paymentContext = new PaymentContext())
                {
                    Payment res = paymentContext.Payments.FirstOrDefault(c => c.PaymentGuid == currentGuid);
                    return res == null;
                }
            }
            catch (Exception e)
            {
                _log.Error(e, "GuidNotExists fail");
                throw;
            }
            
        }
    }
}