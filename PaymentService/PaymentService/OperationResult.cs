﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaymentService
{
    public class OperationResult
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string Note { get; set; }
    }
}