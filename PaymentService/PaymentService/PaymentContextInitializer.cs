﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PaymentService
{
    public class PaymentContextInitializer: CreateDatabaseIfNotExists<PaymentContext>
    {
        protected override void Seed(PaymentContext context)
        {
            List<Vendor> vendors = new List<Vendor>()
            {
                new Vendor(){VendorName = "Megaline"},
                new Vendor(){VendorName = "Homeline"}
            };

            context.Vendors.AddRange(vendors);
            context.SaveChanges();

            List<Account> account = new List<Account>()
            {
                new Account(){Balance = 0, Vendor = vendors.FirstOrDefault(v => v.VendorId == 1)},
                new Account(){Balance = 0, Vendor = vendors.FirstOrDefault(v => v.VendorId == 1)},
                new Account(){Balance = 0, Vendor = vendors.FirstOrDefault(v => v.VendorId == 1)},
                new Account(){Balance = 0, Vendor = vendors.FirstOrDefault(v => v.VendorId == 2)},
            };

            context.Accounts.AddRange(account);
            context.SaveChanges();
        }
    }
}