﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PaymentService
{
    public class PaymentContext:DbContext
    {
        static PaymentContext()
        {
            Database.SetInitializer<PaymentContext>(new PaymentContextInitializer());
        }

        public PaymentContext():base("PaymentServiceDbCon")
        {
            
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<Payment> Payments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .HasRequired(v => v.Vendor)
                .WithMany(a => a.Accounts)
                .HasForeignKey(a => a.VendorId);

            modelBuilder.Entity<Payment>()
                .HasRequired(v => v.Vendor)
                .WithMany(p => p.Payments)
                .HasForeignKey(p => p.VendorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Payment>()
                .HasRequired(a => a.Account)
                .WithMany(p => p.Payments)
                .HasForeignKey(p => p.AccountId)
                .WillCascadeOnDelete(false);
        }
    }
}