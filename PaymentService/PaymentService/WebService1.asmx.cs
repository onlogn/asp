﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Web;
using System.Web.Services;
using NLog;

namespace PaymentService
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        private static Dictionary<int, List<int>> _acc;

        [WebMethod]
        public OperationResult ValidatePayment(int vendorId, int accountId, int sum)
        {
            //PaymentContext pc = new PaymentContext();
            //pc.Database.Initialize(true);

            if (_acc == null)
            {
                _acc = SqlActions.InitializeDictionary();
            }

            OperationResult op = new OperationResult();

            if (!_acc.ContainsKey(vendorId))
            {
                op.ErrorCode = 1;
                op.ErrorMessage = "Unknown Provider!";
                op.Note = $"Provider with id ={vendorId} doesn't exists";
            } else if (!_acc[vendorId].Contains(accountId))
            {
                op.ErrorCode = 2;
                op.ErrorMessage = "Unknown Account";
                op.Note = $"Account with id ={accountId} doesn't exists";
            }
            else if(sum < 10)
            {
                op.ErrorCode = 3;
                op.ErrorMessage = "Incorrect sum";
                op.Note = "MIN SUM = 10";
            } else
            {
                op.ErrorCode = 0;
            }


            return op;
        }

        [WebMethod]
        public string MakePayment(int vendorId, int accountId, int sum, Guid paymentId)
        {

            if (!SqlActions.GuidNotExists(paymentId) || SqlActions.MakePaymentTran(vendorId, accountId, sum, paymentId))
            {
                return $"SUCCESS: Vendor = {vendorId}, Acc = {accountId}, Sum += {sum}, Payment = {paymentId}";
            }

            return "ERROR";
        }
    }
}
