﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaymentService
{
    public class Vendor
    {
        public int VendorId { get; set; }
        public string VendorName { get; set; }

        public List<Account> Accounts { get; set; }
        public List<Payment> Payments { get; set; }
    }
}