﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaymentService
{
    public class Payment
    {
        public int PaymentId { get; set; }
        public int Sum { get; set; }
        public Guid PaymentGuid { get; set; }

        public int VendorId { get; set; }
        public Vendor Vendor { get; set; }

        public int AccountId { get; set; }
        public Account Account { get; set; }

        
    }
}