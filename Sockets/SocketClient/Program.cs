﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketClient
{
    class Program
    {
        private static int port = 13221; 
        private static string serverAddress = "127.0.0.1";
        static void Main(string[] args)
        {
            try
            {
                IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(serverAddress), port);

                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                socket.Connect(ipPoint);

                Console.Write("Input file path:");
                string path = Console.ReadLine();
                string fileName = Path.GetFileName(path);

                if (!File.Exists(path)) throw new Exception("no file");

                FileInfo fileInfo = new FileInfo(path);
                byte[] fileSizeBytes = BitConverter.GetBytes((int) fileInfo.Length);

                byte[] fileNameBytes = Encoding.UTF8.GetBytes(fileName);
                byte[] fileNameLengthBytes = BitConverter.GetBytes(fileNameBytes.Length);

                socket.Send(fileSizeBytes);

                socket.Send(fileNameLengthBytes);

                socket.Send(fileNameBytes);

                byte[] serverAnswer = new byte[1];
                socket.Receive(serverAnswer, 1, SocketFlags.None);

                if (serverAnswer[0] == 0)
                {
                    byte[] fileData = new byte[256];
                    using (BinaryReader reader = new BinaryReader(File.Open(path, FileMode.Open)))
                    {
                        while (reader.PeekChar() > -1)
                        {
                            fileData = reader.ReadBytes(256);
                            socket.Send(fileData);
                        }
                    }

                    socket.Receive(serverAnswer, 1, SocketFlags.None);
                    Console.WriteLine(serverAnswer[0] == 0 ? "Success" : "Fail");
                }
                else
                {
                    Console.WriteLine("File too large...");
                }

                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}

