﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Sockets
{
    class Program
    {
        private static readonly int port = 13221;
        static void Main(string[] args)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
            Socket listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                listenSocket.Bind(ipPoint);
                listenSocket.Listen(10);

                Console.WriteLine("Waiting for connections...");

                while (true)
                {
                    Socket acceptSocket = listenSocket.Accept();

                    byte[] fileSizeBytes = new byte[4]; //int == 4 byte
                    acceptSocket.Receive(fileSizeBytes,4,SocketFlags.None);
                    int fileSize = BitConverter.ToInt32(fileSizeBytes,0);
                    Console.WriteLine($"FileSize = {fileSize} bytes.");

                    byte[] fileNameLengthBytes = new byte[4]; //int == 4 byte
                    acceptSocket.Receive(fileNameLengthBytes, 4, SocketFlags.None);
                    int fileNameLength = BitConverter.ToInt32(fileNameLengthBytes, 0);
                    Console.WriteLine($"FileNameLength = {fileNameLength} bytes");

                    byte[] fileNameBytes = new byte[fileNameLength];
                    acceptSocket.Receive(fileNameBytes, fileNameLength, SocketFlags.None);
                    string fileName = Encoding.UTF8.GetString(fileNameBytes);
                    Console.WriteLine($"FileName = {fileName}");

                    if (fileSize > 5 * 1024 * 1024)
                    {
                        acceptSocket.Send(new byte[] { 1 });
                        Console.WriteLine("File too large");
                    }
                    else
                    {

                        acceptSocket.Send(new byte[] { 0 });
                        byte[] data = new byte[256];
                        try
                        {
                            using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.OpenOrCreate)))
                            {
                                do
                                {
                                    int bytes = acceptSocket.Receive(data);
                                    writer.Write(data.Take(bytes).ToArray());

                                } while (acceptSocket.Available > 0);
                            }

                            acceptSocket.Send(new byte[] {0});
                            Console.WriteLine("Success");
                        }
                        catch (Exception ex)
                        {
                            acceptSocket.Send(new byte[] { 1 });
                            Console.WriteLine(ex.Message);
                        }
                        
                    }

                    acceptSocket.Shutdown(SocketShutdown.Both);
                    acceptSocket.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
