﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LineAsp.Models;

namespace LineAsp.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Gauss()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SolveGauss(double[] elements, int size)
        {
            double[,] arr = new double[size,size+1];
            int cnt = 0;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size + 1; j++)
                {
                    arr[i, j] = elements[cnt++];
                }
            }
            var res = new Matrix(arr);
            return View(res.Solve());
        }

        [HttpPost]
        public ActionResult GetInputArray(int size)
        {
            return PartialView(size);
        }

        [HttpGet]
        public ActionResult SolveEquation()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetAnswerEquation(Equation equation)
        {
            return PartialView(equation.Solve());
        }
    }
}