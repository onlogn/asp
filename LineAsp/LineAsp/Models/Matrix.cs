﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineAsp.Models
{
    public class Matrix
    {
        private double[,] _elements;
        public int Rows { get; private set; }
        public int Cols { get; private set; }

        private const double EPS = 0.000001;

        public Matrix(double[,] elements)
        {
            if (elements == null || elements.GetLength(0) < 1 || elements.GetLength(1) < 1)
            {
                throw new ArgumentException("Data must be correct");
            }
            _elements = elements.Clone() as double[,];
            Rows = _elements.GetLength(0);
            Cols = _elements.GetLength(1);
        }

        public double this[int i, int j]
        {
            get { return _elements[i, j]; }
            set { _elements[i, j] = value; }
        }

        public double[] Solve()
        {
            double[,] a = _elements.Clone() as double[,];
            
            int n = Rows;
            int m = Cols - 1;

            int[] temp = new int[m];
            for (int i = 0; i < n; i++)
            {
                temp[i] = -1;
            }

            for (int col = 0, row = 0; col < m && row < n; ++col)
            {
                int sel = row;
                for (int i = row; i < n; ++i)
                {
                    if (Math.Abs(a[i, col]) > Math.Abs(a[sel, col]))
                    {
                        sel = i;
                    }
                }

                if (Math.Abs((double) a[sel, col]) < EPS)
                {
                    continue;
                }

                for (int i = col; i <= m; ++i)
                {
                    var tmp = a[sel, i];
                    a[sel, i] = a[row, i];
                    a[row, i] = tmp;
                }

                temp[col] = row;

                for (int i = 0; i < n; i++)
                {
                    if (i != row)
                    {
                        double c = a[i, col] / a[row, col];
                        for (int j = 0; j <= m; j++)
                        {
                            a[i, j] -= a[row, j] * c;
                        }
                    }
                }

                ++row;
            }

            double[] ans = new double[m];
            for (int i = 0; i < m; i++)
            {
                ans[i] = 0;
            }

            for (int i = 0; i < m; ++i)
                if (temp[i] != -1)
                    ans[i] = a[temp[i],m] / a[temp[i],i];
            for (int i = 0; i < n; ++i)
            {
                double sum = 0;
                for (int j = 0; j < m; ++j)
                    sum += ans[j] * a[i,j];
                if (Math.Abs(sum - a[i,m]) > EPS)
                    return new double[0];
            }

            for (int i = 0; i < m; ++i)
                if (temp[i] == -1)
                    return new double[0];
            
            return ans;
        }


    }
}