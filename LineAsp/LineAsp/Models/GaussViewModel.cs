﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineAsp.Models
{
    public class GaussViewModel
    {
        public int Size { get; set; }
        public double[,] Elements { get; set; }
    }
}