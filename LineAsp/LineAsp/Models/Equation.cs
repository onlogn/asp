﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LineAsp.Models
{
    public class Equation
    {
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }

        public List<double> Solve()
        {
            List<double> ans = new List<double>();

            double d = B * B - 4 * A * C;

            if (d < 0 || Math.Abs(A) < 0.00001)
            {
                return ans;
            }

            if (d > 0)
            {
                double x1 = (-B + Math.Sqrt(d)) / (2 * A);
                double x2 = (-B - Math.Sqrt(d)) / (2 * A);
                ans.Add(x1);
                ans.Add(x2);
                return ans;
            }

            ans.Add(-B / (2 * A));
            return ans;
        }
    }
}