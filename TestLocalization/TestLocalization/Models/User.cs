﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestLocalization.Models
{
    public class User
    {
        [Required(ErrorMessageResourceType = typeof(Resources.HomeText), ErrorMessageResourceName = "UserIdRequired")]
        public int Id { get; set; }


        [Required(ErrorMessageResourceType = typeof(Resources.HomeText), ErrorMessageResourceName = "UserName")]
        [Display(Name = "UserName", ResourceType = typeof(Resources.HomeText))]
        public string Username { get; set; }
    }
}