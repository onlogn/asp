﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestLocalization.Models;

namespace TestLocalization.Controllers
{
    public class FormController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ShowUser(User u)
        {
            return View(u);
        }
    }
}