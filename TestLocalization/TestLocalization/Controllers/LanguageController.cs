﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace TestLocalization.Controllers
{
    public class LanguageController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Change(string LanguageAbbr)
        {
            string returnUrl = Request.UrlReferrer.AbsolutePath;

            List<string> cultures = new List<string>() { "ru", "en" };
            if (!cultures.Contains(LanguageAbbr))
            {
                LanguageAbbr = "ru";
            }
            
            HttpCookie cookie = Request.Cookies["Language"];
            if (cookie != null)
                cookie.Value = LanguageAbbr;   
            else
            {
                cookie = new HttpCookie("Language");
                cookie.HttpOnly = false;
                cookie.Value = LanguageAbbr;
                cookie.Expires = DateTime.Now.AddMinutes(10);
            }
            Response.Cookies.Add(cookie);
            return Redirect(returnUrl);

        }
    }
}