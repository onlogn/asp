﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebooksStore.Models
{
    public class NotebookDbInitializer: DropCreateDatabaseAlways<NotebookContext>
    {
        protected override void Seed(NotebookContext ctx)
        {
            ctx.Notebooks.Add(new Notebook {Name = "Sony Japan", Manufacturer = "SONY", Price = 500});
            ctx.Notebooks.Add(new Notebook {Name = "Asus t531", Manufacturer = "ASUS", Price = 800});
            ctx.Notebooks.Add(new Notebook {Name = "Acer 123", Manufacturer = "ACER", Price = 650});
        }
    }
}
