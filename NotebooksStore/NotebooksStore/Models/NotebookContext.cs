﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebooksStore.Models
{
    public class NotebookContext: DbContext
    {
        public DbSet<Notebook> Notebooks { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
    }
}
