﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using NotebooksStore.Helpers;

namespace NotebooksStore.Models
{
    public class Notebook//: IValidatableObject
    {
        [HiddenInput(DisplayValue = false)]
        //[ScaffoldColumn(false)]
        public int Id { get; set; }


        //[Required]
        //[Required(ErrorMessage = "Field can not be empty")]
        [Display(Name = "Notebook name")]
        //[UIHint("Url")]
        //[Remote("CheckName", "Annotations", ErrorMessage = "Name is not valid.")]
        //[Notebook(new[] {"SONY", "Acer"})]
        public string Name { get; set; }


        //[Required]
        [Display(Name = "Notebook Manufacturer")]
        //[DataType(DataType.Password)]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "String length must be greather than 2 and less than 25")]
        public string Manufacturer { get; set; }


        //[Required]
        [Display(Name = "Notebook price")]
        //[Range(typeof(decimal), "0.00", "499.99")]
        public int Price { get; set; }


        //[Range(1900, 2018, ErrorMessage = "Incorret year")]
        //public int Year { get; set; }

        //[DataType(DataType.Password)]
        //public string Password { get; set; }
        //[Compare("Password", ErrorMessage = "Passwords are not match")]
        //[DataType(DataType.Password)]
        //public string PasswordConfirm { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var errors = new List<ValidationResult>();
            if (string.IsNullOrWhiteSpace(Name))
            {
                errors.Add(new ValidationResult("incorrect name"));
            }
            if (string.IsNullOrWhiteSpace(Manufacturer))
            {
                errors.Add(new ValidationResult("Enter Manufacturer"));
            }

            return errors;

        }
    }
}
