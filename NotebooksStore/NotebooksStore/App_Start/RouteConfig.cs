﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using NotebooksStore.Helpers;

namespace NotebooksStore
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }


            , namespaces: new[] { "NotebooksStore.Controllers" }

                //defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            ////////////////////////////////////////////////////////////////////////////////////////////
            //routes.MapRoute(
            //name: "Default",
            //url: "{controller}/{action}/{id}"
            //    );
            //var newRoute = new Route("{controller}/{action}", new MvcRouteHandler());
            //routes.Add(newRoute);

            /////////////////////////////////////////////////////////////////////////////////////////////
            //routes.MapRoute(name: "Default", url: "Ru/{controller}/{action}/{id}",
            //    defaults: new { id = UrlParameter.Optional });
            //routes.MapRoute(name: "Default", url: "Ru{controller}/{action}/{id}",
            //    defaults: new { id = UrlParameter.Optional });

            //////////////////////////////////exchange/////////////////////////////////////////////////////
            //routes.MapRoute(name: "Default", url: "{controller}/{action}/{id}", defaults: new { id = UrlParameter.Optional });
            //routes.MapRoute(name: "Default2", url: "Ru{controller}/{action}/{id}", defaults: new { id = UrlParameter.Optional });


            //routes.MapRoute(name: "Default2", url: "Store/Buy", defaults: new { controller = "Home", action = "Index" });

            ///////////////////////////////////////more then 1 parameter/////////////////////////////////////////////
            //routes.MapRoute(name: "Default", url: "{controller}/{action}/{id}/{name}", 
            //    defaults: new { id = UrlParameter.Optional, name = UrlParameter.Optional });

            ////////////////////////////////{*catchall}//////////////////////////////////////////////////
            //routes.MapRoute(name: "Default", url: "{controller}/{action}/{id}/{*catchall}", 
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });

            ///////////////////////////Constraint start with H////////////////////////////////////////////////////
            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
            //    constraints: new { controller = "^H.*" }
            //////constraints: new { controller = "^H.*", id = @"\d{2}" }
            //////constraints: new { controller = "^H.*", id = @"\d{2}", httpMethod=new HttpMethodConstraint("GET") 
            //);

            //////////////////////////////////////////Test CustomConstraint//////////////////////////////////////
            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}/{*catchall}",
            //    defaults: new { controller = "Home", action = "Index" },
            //    constraints: new { id = @"\d{2}", myConstraint = new CustomConstraint("/Home/Index/12") }
            //);

            ////////////////////////////////////Ignore route//////////////////////////////////////////////
            //routes.IgnoreRoute("Home/Index/12");

            ////////////////////////////////////////HTML helper ROUteLink////////////////////////////////////
            //@Html.RouteLink("All Notebooks", "Default", new { action = "Show" })

            /////////////////////////////Duplicate HomeController in Area////////////////////////////////////
            //routes.MapRoute(
            //name: "Default",
            //url: "{controller}/{action}/{id}",
            //defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
            //namespaces: new[] { "NotebooksStore.Controllers" }
            //    );

            //////////////////////////////////////Custom Route Handler///////////////////////////////////////
            //var newRoute = new Route("{controller}/{action}", new MyRouteHandler());
            //routes.Add(newRoute);


            ///////////////////////////////////////////Enable Route attribute///////////////////////////////////
            //routes.MapMvcAttributeRoutes();




        }
    }
}
