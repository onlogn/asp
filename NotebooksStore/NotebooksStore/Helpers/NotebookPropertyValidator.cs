﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NotebooksStore.Models;

namespace NotebooksStore.Helpers
{
    public class NotebookPropertyValidator: ModelValidator
    {
        public NotebookPropertyValidator(ModelMetadata metadata, ControllerContext context)
            : base(metadata, context)
        { }

        public override IEnumerable<ModelValidationResult> Validate(object container)
        {
            var n = container as Notebook;
            if (n != null)
            {
                switch (Metadata.PropertyName)
                {
                    case "Name":
                        if (string.IsNullOrEmpty(n.Name))
                        {
                            return new[]{ new ModelValidationResult { MemberName="Name", Message="Enter Name"}};
                        }
                        break;
                    case "Manufacturer":
                        if (string.IsNullOrEmpty(n.Manufacturer))
                        {
                            return new[]{ new ModelValidationResult { MemberName= "Manufacturer", Message= "Enter Manufacturer" } };
                        }
                        break;
                }
            }
            return Enumerable.Empty<ModelValidationResult>();
        }

    }
}
