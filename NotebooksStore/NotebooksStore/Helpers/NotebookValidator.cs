﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NotebooksStore.Models;

namespace NotebooksStore.Helpers
{
    public class NotebookValidator : ModelValidator
    {
        public NotebookValidator(ModelMetadata metadata, ControllerContext context)
            : base(metadata, context)
        {
        }

        public override IEnumerable<ModelValidationResult> Validate(object container)
        {
            var n = (Notebook) Metadata.Model;
            var errors = new List<ModelValidationResult>();

            if (n.Name == "xxx")
            {
                errors.Add(new ModelValidationResult { MemberName = "", Message = "Incorrect Name" });
            }
            return errors;

        }
    }
}