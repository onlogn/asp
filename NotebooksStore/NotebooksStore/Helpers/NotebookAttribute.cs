﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebooksStore.Helpers
{
    public class NotebookAttribute:ValidationAttribute
    {
        private static string[] notebooks;
        public NotebookAttribute(string[] note)
        {
            notebooks = note;
        }
        public override bool IsValid(object value)
        {
            if (value != null)
            {
                var strval = value.ToString();
                return notebooks.Any(t => strval == t);
            }
            return false;
        }
    }

}

