﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NotebooksStore.Models;

namespace NotebooksStore.Helpers
{
    public class MyValidationProvider : ModelValidatorProvider
    {
        public override IEnumerable<ModelValidator> GetValidators(ModelMetadata metadata, ControllerContext context)
        {
            if (metadata.ContainerType == typeof(Notebook))
            {
                return new ModelValidator[] { new NotebookPropertyValidator(metadata, context) };
            }
            if (metadata.ModelType == typeof(Notebook))
            {
                return new ModelValidator[] { new NotebookValidator(metadata, context) };
            }
            return Enumerable.Empty<ModelValidator>();
        }
    }

}
