﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using NotebooksStore.Models;
using NotebooksStore.Utils;

namespace NotebooksStore.Controllers
{
    //[RoutePrefix("home")]
    public class HomeController : Controller
    {
        NotebookContext ctx = new NotebookContext();


        [HttpPost]
        public ActionResult NotebooksSearch(string name)
        {
            var notebooks = ctx.Notebooks.Where(a => a.Manufacturer.Contains(name)).ToList();
            if (notebooks.Count <= 0)
            {
                return HttpNotFound();
            }
            return PartialView(notebooks);
        }


        [Route("{id:int}/{name}")]
        public string Test(int id, string name)
        {
            return id.ToString() + ". " + name;
        }




        public ActionResult Index()
        {
            //////////////////////////////////
            var controller = RouteData.Values["controller"].ToString();

            var notebooks = ctx.Notebooks;
            //var ttt = notebooks.Where(r => r.Name.Contains("eee")).Select(x => x).ToList();

            ViewBag.Notebooks = notebooks;
            ViewBag.Message = "Partial view";
            //Session["name"] = "ASPNETMVC";

            //return View();
            return View(ctx.Notebooks);
        }


        public async Task<ActionResult> BookList()
        {
            IEnumerable<Notebook> notebooks = await ctx.Notebooks.ToListAsync();
            ViewBag.Books = notebooks;
            return View("Index");
        }


        [HttpGet]
        public ActionResult Buy(int id)
        {
            ViewBag.NotebookId = id;

            return View();
        }

        [HttpPost]
        public string Buy(Purchase purchase)
        {
            purchase.Date = DateTime.Now;

            ctx.Purchases.Add(purchase);
            ctx.SaveChanges();

            return GetName(purchase.Person);

        }

        [HttpGet]
        public ActionResult EditNotebook(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var notebook = ctx.Notebooks.Find(id);
            if (notebook != null)
            {
                return View(notebook);
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult EditNotebook(Notebook notebook)
        {
            ctx.Entry(notebook).State = EntityState.Modified;
            ctx.SaveChanges();
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Notebook notebook)
        {
            ctx.Notebooks.Add(notebook);

            ctx.Entry(notebook).State = EntityState.Added;
            ctx.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var notebook = ctx.Notebooks.Find(id);
            if (notebook == null)
            {
                return HttpNotFound();
            }
            return View(notebook);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var notebook = ctx.Notebooks.Find(id);
            if (notebook == null)
            {
                return HttpNotFound();
            }
            ctx.Notebooks.Remove(notebook);
            ctx.SaveChanges();
            return RedirectToAction("Index");
        }


        #region OldCode

        private string GetName(string str)
        {
            return "Спасибо," + str + ", за покупку!";
        }

        [HttpGet]
        public ContentResult Square(int a /*= 10*/, int h = 3)
        {

            var sss = Request.Params["a"];
            var s = a * h / 2.0;
            return Content("<h2>Площадь треугольника с основанием " + a +
                   " и высотой " + h + " равна " + s + "</h2>");
        }

        public ActionResult GetHtml()
        {
            return new HtmlResult("<h2>Привет мир!</h2>");
        }

        public ActionResult GetImage()
        {
            string path = @"../Img/20140906_194528.png";
            return new ImageResult(path);
        }

        public ViewResult ViewDataTest()
        {
            ViewData["Head"] = "Привет мир!";
            //ViewBag.Head = "Привет мир!";
            return View("~/Views/ViewDataTest/Index.cshtml");
        }

        public RedirectToRouteResult RedirectTest()
        {
            return RedirectToRoute(new { controller = "Home", action = "Index" });
            //return RedirectPermanent("/Home/Index");
        }

        public RedirectToRouteResult RedirectToRouteTest()
        {
            return RedirectToRoute(new { controller = "Home", action = "Index" });
        }

        public RedirectToRouteResult RedirectToActionTest()
        {
            return RedirectToAction("Square", "Home", new { a = 10, h = 12 });
        }

        public ActionResult CheckAge(int age)
        {
            if (age < 21)
            {
                return new HttpStatusCodeResult(404);
                return HttpNotFound();
                //return new HttpUnauthorizedResult();
            }
            return View("~/Views/Home/Index.cshtml");
        }


        public FileResult GetFile()
        {
            //var file_path = Server.MapPath("~/Files/front page.pdf");
            var file_path = @"D:\front page.pdf";
            var file_type = "application/pdf";
            var file_name = "front page.pdf";
            return File(file_path, file_type, file_name);
        }

        public FileResult GetBytes()
        {
            var path = Server.MapPath("~/Files/front page.pdf");
            var mas = System.IO.File.ReadAllBytes(path);
            var file_type = "application/pdf";
            var file_name = "front page.pdf";
            return File(mas, file_type, file_name);
        }

        public FileResult GetStream()
        {
            var path = Server.MapPath("~/Files/front page.pdf");
            var fs = new FileStream(path, FileMode.Open);
            var file_type = "application/pdf";
            var file_name = "front page.pdf";
            return File(fs, file_type, file_name);
        }


        public string RequestTest()
        {
            var browser = HttpContext.Request.Browser.Browser;
            var user_agent = HttpContext.Request.UserAgent;
            var url = HttpContext.Request.RawUrl;
            var ip = HttpContext.Request.UserHostAddress;
            var referrer = HttpContext.Request.UrlReferrer == null ? "" : HttpContext.Request.UrlReferrer.AbsoluteUri;
            return "<p>Browser: " + browser + "</p><p>User-Agent: " + user_agent + "</p><p>Url запроса: " + url +
                   "</p><p>Реферер: " + referrer + "</p><p>IP-адрес: " + ip + "</p>";
        }


        public string ContextData()
        {
            HttpContext.Response.Write("<h1>Hello World</h1>");

            var user_agent = HttpContext.Request.UserAgent;
            var url = HttpContext.Request.RawUrl;
            var ip = HttpContext.Request.UserHostAddress;
            var referrer = HttpContext.Request.UrlReferrer == null ? "" : HttpContext.Request.UrlReferrer.AbsoluteUri;
            return "<p>User-Agent: " + user_agent + "</p><p>Url запроса: " + url +
                   "</p><p>Реферер: " + referrer + "</p><p>IP-адрес: " + ip + "</p>";
        }


        public string CookiesTest()
        {
            var id = HttpContext.Request.Cookies["id"]?.Value;

            HttpContext.Response.Cookies["id"].Value = "ca-4353w";

            return $"<h1>Cookies {id}</h1>";

        }


        public string GetSessionName()
        {
            var val = Session["name"];
            return val.ToString();
        }


        public ActionResult Partial()
        {
            ViewBag.Message = "Partial view";
            return PartialView();
        }


        #endregion


    }
}