﻿using System.Web.Mvc;

namespace NotebooksStore.Controllers
{
    [RoutePrefix("home")]
    public class RouteController : Controller
    {
        [Route("{id:int}/{name}")]
        public string Test(int id, string name)
        {
            return id.ToString() + ". " + name;
        }
        [Route("{id:int}")]
        public string Test1(int id)
        {
            return id.ToString();
        }
        [Route("~/xxx/ignoreprefix/{id:int}")]
        public string IgnorePrefix(int id)
        {
            return id.ToString();
        }
    }

}
