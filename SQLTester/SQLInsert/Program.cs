﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLInsert
{
    class Program
    {
        private static Random _rnd = new Random();
        static void Main(string[] args)
        {
            SQLTesterEntities db = new SQLTesterEntities();

            List<Record> records = new List<Record>();

            for (long i = 0; i < 2000000; i++)
            {
                records.Add(new Record()
                {
                    Date = DateTime.Now,
                    Price = _rnd.Next(0, 10000),
                    Name = GenerateRndStr(_rnd.Next(120))
                });
                
            }
            var time = new Stopwatch();
            time.Start();
            db.BulkInsert(records);
            db.BulkSaveChanges();
            time.Stop();
            Console.WriteLine(time.Elapsed.Seconds);
        }

        private static string GenerateRndStr(int len)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < len; i++)
            {
                sb.Append(chars[_rnd.Next(chars.Length)]);
            }

            return sb.ToString();
        }
    }
}
