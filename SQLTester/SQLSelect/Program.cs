﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityBulkInsert;

namespace SQLSelect
{
    class Program
    {
        static void Main(string[] args)
        {
            SQLTesterEntities db = new SQLTesterEntities();

            var timer = new Stopwatch();

            timer.Start();
            var res = db.Records.Where(c => c.Name.StartsWith("ABC")).ToList();
            timer.Stop();

            Console.WriteLine(timer.Elapsed.TotalSeconds);
        }
    }
}
