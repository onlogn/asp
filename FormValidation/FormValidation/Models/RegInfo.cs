﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FormValidation.Models
{
    public class RegInfo
    {
        [Required(ErrorMessage = "заполните!")]
        [Display(Name = "Имя")]
        [StringLength(10, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 10 символов")]
        [RegularExpression(@"^[А-Я][а-я]+$", ErrorMessage = "ненене")]
        public string Name { get; set; }

        [Required(ErrorMessage = "заполните!")]
        [Display(Name = "Фамилия")]
        [StringLength(15, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 15 символов")]
        [RegularExpression(@"^[A-Z][a-z]+$", ErrorMessage = "ненене")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "заполните!")]
        [Display(Name = "Почта")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@mail\.ru", ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }

        [Required(ErrorMessage = "заполните!")]
        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$", ErrorMessage = "Minimum eight characters, at least one letter, one number and one special character")]
        public string Password { get; set; }

        [Required(ErrorMessage = "заполните!")]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        public string PassConfirm { get; set; }


    }
}

