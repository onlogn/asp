﻿using System.Web.Mvc;
using TestFormProject.Models;

namespace TestFormProject.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(User user)
        {
            return RedirectToAction("Details",user);
        }

        [HttpGet]
        public ActionResult Details(User user)
        {
            return View(user);
        }
    }
}