﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GetPostTester.Models;

namespace GetPostTester.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index(int? a, int? b)
        {
            a = a ?? 0;
            b = b ?? 0;
            ViewBag.c = a + b;
            return View();
        }

        [HttpPost]
        public ActionResult ShowUserInfo(User user)
        {
            return View(user);
        }
    }
}