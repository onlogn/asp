﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GetPostTester.Models
{
    public class User
    {
        public string UserName { get; set; }
        public string LastName { get; set; }
    }
}