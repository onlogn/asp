﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SOAPClient.ServiceReference1 {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Student", Namespace="http://tempuri.org/")]
    [System.SerializableAttribute()]
    public partial class Student : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private int IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StudentNameField;
        
        private System.Nullable<System.DateTime> BirthDateTimeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string StudentName {
            get {
                return this.StudentNameField;
            }
            set {
                if ((object.ReferenceEquals(this.StudentNameField, value) != true)) {
                    this.StudentNameField = value;
                    this.RaisePropertyChanged("StudentName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public System.Nullable<System.DateTime> BirthDateTime {
            get {
                return this.BirthDateTimeField;
            }
            set {
                if ((this.BirthDateTimeField.Equals(value) != true)) {
                    this.BirthDateTimeField = value;
                    this.RaisePropertyChanged("BirthDateTime");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.WebService1Soap")]
    public interface WebService1Soap {
        
        // CODEGEN: Generating message contract since element name student from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/InsertData", ReplyAction="*")]
        SOAPClient.ServiceReference1.InsertDataResponse InsertData(SOAPClient.ServiceReference1.InsertDataRequest request);
        
        // CODEGEN: Generating message contract since element name student from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/UpdateData", ReplyAction="*")]
        SOAPClient.ServiceReference1.UpdateDataResponse UpdateData(SOAPClient.ServiceReference1.UpdateDataRequest request);
        
        // CODEGEN: Generating message contract since element name student from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/DeleteData", ReplyAction="*")]
        SOAPClient.ServiceReference1.DeleteDataResponse DeleteData(SOAPClient.ServiceReference1.DeleteDataRequest request);
        
        // CODEGEN: Generating message contract since element name SelectDataResult from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SelectData", ReplyAction="*")]
        SOAPClient.ServiceReference1.SelectDataResponse SelectData(SOAPClient.ServiceReference1.SelectDataRequest request);
        
        // CODEGEN: Generating message contract since element name GetLastErrorResult from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetLastError", ReplyAction="*")]
        SOAPClient.ServiceReference1.GetLastErrorResponse GetLastError(SOAPClient.ServiceReference1.GetLastErrorRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class InsertDataRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="InsertData", Namespace="http://tempuri.org/", Order=0)]
        public SOAPClient.ServiceReference1.InsertDataRequestBody Body;
        
        public InsertDataRequest() {
        }
        
        public InsertDataRequest(SOAPClient.ServiceReference1.InsertDataRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class InsertDataRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public SOAPClient.ServiceReference1.Student student;
        
        public InsertDataRequestBody() {
        }
        
        public InsertDataRequestBody(SOAPClient.ServiceReference1.Student student) {
            this.student = student;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class InsertDataResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="InsertDataResponse", Namespace="http://tempuri.org/", Order=0)]
        public SOAPClient.ServiceReference1.InsertDataResponseBody Body;
        
        public InsertDataResponse() {
        }
        
        public InsertDataResponse(SOAPClient.ServiceReference1.InsertDataResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class InsertDataResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public bool InsertDataResult;
        
        public InsertDataResponseBody() {
        }
        
        public InsertDataResponseBody(bool InsertDataResult) {
            this.InsertDataResult = InsertDataResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class UpdateDataRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="UpdateData", Namespace="http://tempuri.org/", Order=0)]
        public SOAPClient.ServiceReference1.UpdateDataRequestBody Body;
        
        public UpdateDataRequest() {
        }
        
        public UpdateDataRequest(SOAPClient.ServiceReference1.UpdateDataRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class UpdateDataRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public SOAPClient.ServiceReference1.Student student;
        
        public UpdateDataRequestBody() {
        }
        
        public UpdateDataRequestBody(SOAPClient.ServiceReference1.Student student) {
            this.student = student;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class UpdateDataResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="UpdateDataResponse", Namespace="http://tempuri.org/", Order=0)]
        public SOAPClient.ServiceReference1.UpdateDataResponseBody Body;
        
        public UpdateDataResponse() {
        }
        
        public UpdateDataResponse(SOAPClient.ServiceReference1.UpdateDataResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class UpdateDataResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public bool UpdateDataResult;
        
        public UpdateDataResponseBody() {
        }
        
        public UpdateDataResponseBody(bool UpdateDataResult) {
            this.UpdateDataResult = UpdateDataResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class DeleteDataRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="DeleteData", Namespace="http://tempuri.org/", Order=0)]
        public SOAPClient.ServiceReference1.DeleteDataRequestBody Body;
        
        public DeleteDataRequest() {
        }
        
        public DeleteDataRequest(SOAPClient.ServiceReference1.DeleteDataRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class DeleteDataRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public SOAPClient.ServiceReference1.Student student;
        
        public DeleteDataRequestBody() {
        }
        
        public DeleteDataRequestBody(SOAPClient.ServiceReference1.Student student) {
            this.student = student;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class DeleteDataResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="DeleteDataResponse", Namespace="http://tempuri.org/", Order=0)]
        public SOAPClient.ServiceReference1.DeleteDataResponseBody Body;
        
        public DeleteDataResponse() {
        }
        
        public DeleteDataResponse(SOAPClient.ServiceReference1.DeleteDataResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class DeleteDataResponseBody {
        
        public DeleteDataResponseBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class SelectDataRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="SelectData", Namespace="http://tempuri.org/", Order=0)]
        public SOAPClient.ServiceReference1.SelectDataRequestBody Body;
        
        public SelectDataRequest() {
        }
        
        public SelectDataRequest(SOAPClient.ServiceReference1.SelectDataRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class SelectDataRequestBody {
        
        public SelectDataRequestBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class SelectDataResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="SelectDataResponse", Namespace="http://tempuri.org/", Order=0)]
        public SOAPClient.ServiceReference1.SelectDataResponseBody Body;
        
        public SelectDataResponse() {
        }
        
        public SelectDataResponse(SOAPClient.ServiceReference1.SelectDataResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class SelectDataResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public SOAPClient.ServiceReference1.Student[] SelectDataResult;
        
        public SelectDataResponseBody() {
        }
        
        public SelectDataResponseBody(SOAPClient.ServiceReference1.Student[] SelectDataResult) {
            this.SelectDataResult = SelectDataResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetLastErrorRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetLastError", Namespace="http://tempuri.org/", Order=0)]
        public SOAPClient.ServiceReference1.GetLastErrorRequestBody Body;
        
        public GetLastErrorRequest() {
        }
        
        public GetLastErrorRequest(SOAPClient.ServiceReference1.GetLastErrorRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class GetLastErrorRequestBody {
        
        public GetLastErrorRequestBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetLastErrorResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetLastErrorResponse", Namespace="http://tempuri.org/", Order=0)]
        public SOAPClient.ServiceReference1.GetLastErrorResponseBody Body;
        
        public GetLastErrorResponse() {
        }
        
        public GetLastErrorResponse(SOAPClient.ServiceReference1.GetLastErrorResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class GetLastErrorResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string GetLastErrorResult;
        
        public GetLastErrorResponseBody() {
        }
        
        public GetLastErrorResponseBody(string GetLastErrorResult) {
            this.GetLastErrorResult = GetLastErrorResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface WebService1SoapChannel : SOAPClient.ServiceReference1.WebService1Soap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WebService1SoapClient : System.ServiceModel.ClientBase<SOAPClient.ServiceReference1.WebService1Soap>, SOAPClient.ServiceReference1.WebService1Soap {
        
        public WebService1SoapClient() {
        }
        
        public WebService1SoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WebService1SoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WebService1SoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WebService1SoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SOAPClient.ServiceReference1.InsertDataResponse SOAPClient.ServiceReference1.WebService1Soap.InsertData(SOAPClient.ServiceReference1.InsertDataRequest request) {
            return base.Channel.InsertData(request);
        }
        
        public bool InsertData(SOAPClient.ServiceReference1.Student student) {
            SOAPClient.ServiceReference1.InsertDataRequest inValue = new SOAPClient.ServiceReference1.InsertDataRequest();
            inValue.Body = new SOAPClient.ServiceReference1.InsertDataRequestBody();
            inValue.Body.student = student;
            SOAPClient.ServiceReference1.InsertDataResponse retVal = ((SOAPClient.ServiceReference1.WebService1Soap)(this)).InsertData(inValue);
            return retVal.Body.InsertDataResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SOAPClient.ServiceReference1.UpdateDataResponse SOAPClient.ServiceReference1.WebService1Soap.UpdateData(SOAPClient.ServiceReference1.UpdateDataRequest request) {
            return base.Channel.UpdateData(request);
        }
        
        public bool UpdateData(SOAPClient.ServiceReference1.Student student) {
            SOAPClient.ServiceReference1.UpdateDataRequest inValue = new SOAPClient.ServiceReference1.UpdateDataRequest();
            inValue.Body = new SOAPClient.ServiceReference1.UpdateDataRequestBody();
            inValue.Body.student = student;
            SOAPClient.ServiceReference1.UpdateDataResponse retVal = ((SOAPClient.ServiceReference1.WebService1Soap)(this)).UpdateData(inValue);
            return retVal.Body.UpdateDataResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SOAPClient.ServiceReference1.DeleteDataResponse SOAPClient.ServiceReference1.WebService1Soap.DeleteData(SOAPClient.ServiceReference1.DeleteDataRequest request) {
            return base.Channel.DeleteData(request);
        }
        
        public void DeleteData(SOAPClient.ServiceReference1.Student student) {
            SOAPClient.ServiceReference1.DeleteDataRequest inValue = new SOAPClient.ServiceReference1.DeleteDataRequest();
            inValue.Body = new SOAPClient.ServiceReference1.DeleteDataRequestBody();
            inValue.Body.student = student;
            SOAPClient.ServiceReference1.DeleteDataResponse retVal = ((SOAPClient.ServiceReference1.WebService1Soap)(this)).DeleteData(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SOAPClient.ServiceReference1.SelectDataResponse SOAPClient.ServiceReference1.WebService1Soap.SelectData(SOAPClient.ServiceReference1.SelectDataRequest request) {
            return base.Channel.SelectData(request);
        }
        
        public SOAPClient.ServiceReference1.Student[] SelectData() {
            SOAPClient.ServiceReference1.SelectDataRequest inValue = new SOAPClient.ServiceReference1.SelectDataRequest();
            inValue.Body = new SOAPClient.ServiceReference1.SelectDataRequestBody();
            SOAPClient.ServiceReference1.SelectDataResponse retVal = ((SOAPClient.ServiceReference1.WebService1Soap)(this)).SelectData(inValue);
            return retVal.Body.SelectDataResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SOAPClient.ServiceReference1.GetLastErrorResponse SOAPClient.ServiceReference1.WebService1Soap.GetLastError(SOAPClient.ServiceReference1.GetLastErrorRequest request) {
            return base.Channel.GetLastError(request);
        }
        
        public string GetLastError() {
            SOAPClient.ServiceReference1.GetLastErrorRequest inValue = new SOAPClient.ServiceReference1.GetLastErrorRequest();
            inValue.Body = new SOAPClient.ServiceReference1.GetLastErrorRequestBody();
            SOAPClient.ServiceReference1.GetLastErrorResponse retVal = ((SOAPClient.ServiceReference1.WebService1Soap)(this)).GetLastError(inValue);
            return retVal.Body.GetLastErrorResult;
        }
    }
}
