﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Students
{
    public class Student
    {
        public int Id { get; set; }
        public string StudentName { get; set; }
        public DateTime? BirthDateTime { get; set; }
    }
}
