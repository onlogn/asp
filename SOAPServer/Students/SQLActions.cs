﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Students
{
    public class SqlActions : IDisposable
    {
        private readonly string _connectionStr;
        private SqlConnection _connection;
        public SqlActions(string conStr)
        {
            _connectionStr = conStr;

            _connection = new SqlConnection(_connectionStr);
            _connection.Open();
        }

        public bool InsertData(Student student)
        {
            string dayofBirth = student.BirthDateTime.HasValue ? "@dayOfBirth" : "NULL";
            string query = $"INSERT INTO Students(StudentName,BirthDate) VALUES(@name,{dayofBirth})";


            SqlCommand cmd = new SqlCommand(query, _connection);

            SqlParameter pName = new SqlParameter("@name", student.StudentName);
            cmd.Parameters.Add(pName);

            if (student.BirthDateTime != null)
            {
                SqlParameter pDate = new SqlParameter("@dayOfBirth", student.BirthDateTime);
                cmd.Parameters.Add(pDate);
            }

            return cmd.ExecuteNonQuery() > 0;

        }

        public bool UpdateData(Student student)
        {
            string query = "UPDATE Students SET StudentName=@name, BirthDate=@dayOfBirth WHERE Id=@id";
            
            SqlCommand cmd = new SqlCommand(query, _connection);

            SqlParameter pName = new SqlParameter("@name", student.StudentName);
            cmd.Parameters.Add(pName);

            SqlParameter pDate = new SqlParameter("@dayOfBirth", student.BirthDateTime);
            cmd.Parameters.Add(pDate);

            SqlParameter pId = new SqlParameter("@id", student.Id);
            cmd.Parameters.Add(pId);

            return cmd.ExecuteNonQuery() > 0;

        }

        public bool DeleteData(int id)
        {
            string query = "DELETE FROM Students WHERE Id=@id";

            SqlCommand cmd = new SqlCommand(query, _connection);

            SqlParameter pId = new SqlParameter("@id", id);
            cmd.Parameters.Add(pId);

            return cmd.ExecuteNonQuery() > 0;

        }

        public List<Student> SelectData()
        {
            List<Student> students = new List<Student>();

            string query = "SELECT Id, StudentName, BirthDate FROM Students";

            SqlCommand cmd = new SqlCommand(query, _connection);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    students.Add(new Student()
                    {
                        Id = reader.GetInt32(0),
                        StudentName = reader.GetString(1),
                        BirthDateTime = reader.IsDBNull(2) ? (DateTime?)null : reader.GetDateTime(2)
                    });
                }
            }

            return students;
        }

        public void Dispose()
        {
            _connection?.Dispose();
        }
    }
}
