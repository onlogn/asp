﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Logger
{
    public static class Log
    {
        private enum LogType
        {
            Error,
            Warning,
            Info
        }

        private static object _errorLock = new Object();
        private static object _warningLock = new Object();
        private static object _infoLock = new Object();

        private const string LogFileError = @"G:\error_";
        private const string LogFileWarning = @"G:\warning_";
        private const string LogFileInfo = @"G:\info_";

        private static string _lastError = null;

        public static void ErrorLog(Exception e)
        {
            string msg = $"Message:{e.Message}\nStackTrace:{e.StackTrace}";
            LogMsg(msg, LogType.Error);
            _lastError = msg;
        }

        public static void InfoLog(string msg)
        {
            LogMsg(msg, LogType.Info);
        }

        public static void WarningLog(string msg)
        {
            LogMsg(msg,LogType.Warning);
        }

        private static void LogMsg(string msg, LogType type)
        {
            lock (GetLockObject(type))
            {
                var curDateTime = DateTime.Now;
                string curDateStr = curDateTime.ToString("yyyy-MM-dd");

            
                string content = $"{curDateTime} / {msg}\n";
                string curFile = GetFileName(type);
                File.AppendAllText($"{curFile}{curDateStr}.txt", content);
            }
        }

        private static object GetLockObject(LogType type)
        {
            switch (type)
            {
                case LogType.Error: return _errorLock;
                case LogType.Info: return _infoLock;
                case LogType.Warning: return _warningLock;
                default: return null;
            }
        }

        private static string GetFileName(LogType type)
        {
            switch (type)
            {
                case LogType.Error: return LogFileError;
                case LogType.Info: return LogFileInfo;
                case LogType.Warning: return LogFileWarning;
                default: return null;
            }
        }

        public static string GetLastError()
        {
            return _lastError;
        }
    }
}
