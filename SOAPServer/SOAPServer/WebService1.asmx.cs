﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using Logger;
using Students;

namespace SOAPServer
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {
        private string _conStr = ConfigurationManager.ConnectionStrings["StudentDbCon"].ConnectionString;
        [WebMethod]
        public bool InsertData(Student student)
        {
            try
            {
                using (SqlActions sql = new SqlActions(_conStr))
                {
                    sql.InsertData(student);
                    Log.InfoLog($"Student {student.StudentName} was inserted");
                    if (student.BirthDateTime == null)
                    {
                        Log.WarningLog($"Student without DoB was Added ({student.StudentName})");
                    }

                    return true;
                }
                
            }
            catch (Exception e)
            {
                Log.ErrorLog(e);
                return false;
            }
        }

        [WebMethod]
        public bool UpdateData(Student student)
        {
            try
            {
                using (SqlActions sql = new SqlActions(_conStr))
                {
                    sql.UpdateData(student);
                    Log.InfoLog($"Student {student.Id} was updated");
                    return true;
                }
            }
            catch (Exception e)
            {
                Log.ErrorLog(e);
                throw;
            }
            
        }

        [WebMethod]
        public void DeleteData(Student student)
        {
            try
            {
                using (SqlActions sql = new SqlActions(_conStr))
                {
                    sql.DeleteData(student.Id);
                    Log.InfoLog($"Student {student.Id} was deleted");
                }
            }
            catch (Exception e)
            {
                Log.ErrorLog(e);
                throw;
            }
            
        }

        [WebMethod]
        public List<Student> SelectData()
        {
            
            try
            {
                using (SqlActions sql = new SqlActions(_conStr))
                {
                    return sql.SelectData();
                }
            }
            catch (Exception e)
            {
                Log.ErrorLog(e);
                throw;
            }
            
        }

        [WebMethod]
        public string GetLastError()
        {
            return Log.GetLastError();
        }
    }
}
