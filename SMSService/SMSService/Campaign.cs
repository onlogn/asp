//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSService
{
    using System;
    using System.Collections.Generic;
    
    public partial class Campaign
    {
        public int Id { get; set; }
        public System.DateTime StartDateTime { get; set; }
        public int MessageTypeId { get; set; }
        public int StatusId { get; set; }
        public int GroupId { get; set; }
        public string MessageText { get; set; }
    }
}
