﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMSService
{
    public class SMSService
    {
        public int NotCompleteStatus { get; }
        public  int CompletedStatus { get; }
        public int SMSTypeId { get; }


        public SMSService(int notCmp, int completed, int sms)
        {
            NotCompleteStatus = notCmp;
            CompletedStatus = completed;
            SMSTypeId = sms;
        }

        public void DoTask()
        {
            DateTime dtNow = DateTime.Now;

            using (PartOfDbForSMSTestsEntities db = new PartOfDbForSMSTestsEntities())
            {
                var cmpList =  db.Campaigns.Where(c => c.MessageTypeId == SMSTypeId && c.StatusId == NotCompleteStatus && c.StartDateTime < dtNow ).ToList();

                foreach (var campaign in cmpList)
                {
                    List<string> clients = GetClientsList(campaign);
                    if (SendSMS(clients))
                    {
                        var currentCampaign = db.Campaigns.FirstOrDefault(c => c.Id == campaign.Id);
                        if (currentCampaign == null) continue;
                        currentCampaign.StatusId = CompletedStatus;
                        db.SaveChanges();
                    }
                }

            }
        }

        private List<string> GetClientsList(Campaign c)
        {
            //тут логика из бд готовой
            
            return new List<string>(){"322223"};
        }

        private bool SendSMS(List<string> clients)
        {
            return true;
        }

    }
}
