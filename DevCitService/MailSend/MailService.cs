﻿using DbEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MailSend
{
    public class MailService
    {
        public static void StartService()
        {
            TimerCallback search = new TimerCallback(SearchCampaigns);
            int waitMins = 60;
            int interval = 60000 * waitMins;
            Timer timer = new Timer(search, null, 0, interval);
            Console.ReadKey();
        }

        public static void SearchCampaigns(object obj)
        {
            using (NewsletterEntities db = new NewsletterEntities())
            {
                var toSendCampaigns = db.Campaigns.Where(camp => camp.Status.Status1 == "Scheduled" && camp.StartDateTime >= DateTime.Now);
                foreach (var campaign in toSendCampaigns)
                {
                    campaign.StatusId = db.Statuses.First(stat => stat.Status1 == "Process").Id;
                    db.SaveChanges();
                    foreach (var listener in campaign.Groupe.Listeners)
                    {
                        SendToMail(listener.Student.Email, campaign.MessageText);
                    }
                    campaign.StatusId = db.Statuses.First(stat => stat.Status1 == "Finished").Id;
                    db.SaveChanges();
                }
            }
        }

        private static void SendToMail(string email, string messageText)
        {
            #region Replace me
            Console.WriteLine(email + " " + messageText);
            #endregion
        }
    }
}
