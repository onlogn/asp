﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbEntities;

namespace SMSSend
{
    public class SMSService
    {
        public int NotCompleteStatus { get; }
        public int CompletedStatus { get; }
        public int SMSTypeId { get; }


        public SMSService(int notCompleted, int completed, int smsMsgType)
        {
            NotCompleteStatus = notCompleted;
            CompletedStatus = completed;
            SMSTypeId = smsMsgType;
        }

        public void DoTask()
        {
            DateTime dtNow = DateTime.Now;

            using (NewsletterEntities db = new NewsletterEntities())
            {
                var cmpList = db.Campaigns.Where(c => c.MessageTypeId == SMSTypeId && c.StatusId == NotCompleteStatus && c.StartDateTime < dtNow).ToList();

                foreach (var campaign in cmpList)
                {
                    List<string> clients = GetClientsList(campaign);
                    if (SendSMS(clients))
                    {
                        campaign.StatusId = CompletedStatus;
                        db.SaveChanges();
                    }
                }
            }
        }

        private List<string> GetClientsList(Campaign c)
        {

            using (NewsletterEntities db = new NewsletterEntities())
            {
                return db.Students
                    .Include(t => t.Listeners)
                    .Where(t => t.Listeners.Any(s => s.GroupId == c.GroupId))
                    .Select(t => t.Phone)
                    .Distinct()
                    .ToList();

            }
        }

        private bool SendSMS(List<string> clients)
        {
            return true;
        }

    }
}
