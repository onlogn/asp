﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbEntities;
using NLog;

namespace SMSSend
{
    public class SMSService
    {
        static Logger log = LogManager.GetCurrentClassLogger();
        public int NotCompleteStatus { get; }
        public int CompletedStatus { get; }
        public int SMSTypeId { get; }


        public SMSService(int notCompleted, int completed, int smsMsgType)
        {
            NotCompleteStatus = notCompleted;
            CompletedStatus = completed;
            SMSTypeId = smsMsgType;
        }

        public void DoTask()
        {
            DateTime dtNow = DateTime.Now;

            using (NewsletterEntities db = new NewsletterEntities())
            {
                log.Info("Search campaigns to send");
                var cmpList = db.Campaigns.Where(c => c.MessageTypeId == SMSTypeId && c.StatusId == NotCompleteStatus && c.StartDateTime < dtNow).ToList();

                log.Info("To count campaigns send " + cmpList.Count().ToString());
                foreach (var campaign in cmpList)
                {
                    List<string> clients = GetClientsList(campaign);
                    log.Info("Send campaigns ID=" + campaign.Id.ToString());
                    if (SendSMS(clients))
                    {
                        campaign.StatusId = CompletedStatus;
                        db.SaveChanges();
                    }
                }
            }
        }

        private List<string> GetClientsList(Campaign c)
        {

            using (NewsletterEntities db = new NewsletterEntities())
            {
                return db.Students
                    .Include(t => t.Listeners)
                    .Where(t => t.Listeners.Any(s => s.GroupId == c.GroupId))
                    .Select(t => t.Phone)
                    .Distinct()
                    .ToList();

            }
        }

        private bool SendSMS(List<string> clients)
        {
            return true;
        }

    }
}
