﻿using DbEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace MailSend
{
    public class MailService
    {
        static Logger log = LogManager.GetCurrentClassLogger();
        public static void StartService()
        {
            log.Info("Start MailService");
            TimerCallback search = new TimerCallback(SearchCampaigns);
            int waitMins = 60;
            int interval = 60000 * waitMins;
            try
            {
                Timer timer = new Timer(search, null, 0, interval);
            }
            catch (Exception ex)
            {
                log.Error("Error MailService", ex.StackTrace);
            }
            Console.ReadKey();
            log.Warn("Stop MailService");
        }

        public static void SearchCampaigns(object obj)
        {
            log.Info("Search campaigns to send");
            using (NewsletterEntities db = new NewsletterEntities())
            {
                var toSendCampaigns = db.Campaigns.Where(camp => camp.Status.Status1 == "Scheduled" && camp.StartDateTime >= DateTime.Now);
                log.Info("To count campaigns send " + toSendCampaigns.Count().ToString());
                foreach (var campaign in toSendCampaigns)
                {
                    campaign.StatusId = db.Statuses.First(stat => stat.Status1 == "Process").Id;
                    db.SaveChanges();
                    log.Info("Send campaigns ID=" + campaign.Id.ToString());
                    foreach (var listener in campaign.Groupe.Listeners)
                    {
                        try
                        {
                            SendToMail(listener.Student.Email, campaign.MessageText);
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error send to " + listener.Student.Email, ex.StackTrace);
                        }
                    }
                    campaign.StatusId = db.Statuses.First(stat => stat.Status1 == "Finished").Id;
                    db.SaveChanges();
                    log.Info("Finished Send campaigns ID=" + campaign.Id.ToString());
                }
            }
        }

        private static void SendToMail(string email, string messageText)
        {
            #region Replace me
            Console.WriteLine(email + " " + messageText);
            log.Info(email + " " + messageText);
            #endregion
        }
    }
}
