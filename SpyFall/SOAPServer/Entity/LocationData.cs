﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOAPServer.Entity
{
    public class LocationData
    {
        public int LocationDataId { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public DateTime DateTime { get; set; }

    }
}