﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SOAPServer.Entity
{
    public class LocationDataContext : DbContext
    {
        public LocationDataContext():base("Locations")
        {
            
        }

        public DbSet<LocationData> LocationDatas { get; set; }
    }
}