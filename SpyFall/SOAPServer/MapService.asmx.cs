﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MapUtils;
using SOAPServer.Entity;

namespace SOAPServer
{
    /// <summary>
    /// Summary description for MapService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MapService : System.Web.Services.WebService
    {

        [WebMethod]
        public void AddLocation(Location location)
        {
            using (LocationDataContext db = new LocationDataContext())
            {
                db.LocationDatas.Add(new LocationData(){X = location.X, Y = location.Y, DateTime = location.DateTime});
                db.SaveChanges();
            }
        }

        [WebMethod]
        public List<Location> GetLocations()
        {
            using (LocationDataContext db = new LocationDataContext())
            {
                return db.LocationDatas.Select(c => new Location() {X = c.X, Y = c.Y, DateTime = c.DateTime}).ToList();
            }
        }

        [WebMethod]
        public void RemoveAllLocations()
        {
            using (LocationDataContext db = new LocationDataContext())
            {
                db.Database.ExecuteSqlCommand("TRUNCATE TABLE LocationDatas");
            }
        }
    }
}
