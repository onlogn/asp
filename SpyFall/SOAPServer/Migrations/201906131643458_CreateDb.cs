namespace SOAPServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LocationDatas",
                c => new
                    {
                        LocationDataId = c.Int(nullable: false, identity: true),
                        X = c.Double(nullable: false),
                        Y = c.Double(nullable: false),
                        DateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.LocationDataId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LocationDatas");
        }
    }
}
