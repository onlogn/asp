﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapUtils
{
    public class Location
    {
        public double X { get; set; }
        public double Y { get; set; }
        public DateTime DateTime { get; set; }
    }
}
