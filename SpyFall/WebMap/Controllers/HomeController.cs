﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMap.MapServiceReference;

namespace WebMap.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var service = new MapServiceReference.MapService();
            List<Location> list = service.GetLocations().ToList();

            return View(list);
        }
    }
}