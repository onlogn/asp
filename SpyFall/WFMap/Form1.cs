﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFMap.MapServiceReference;

namespace WFMap
{
    public partial class ClickerForm : Form
    {
        private MapService _service;
        public ClickerForm()
        {
            InitializeComponent();
            _service = new MapServiceReference.MapService();
        }

        private void PictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            _service.AddLocation(new Location(){X = e.X, Y = e.Y, DateTime = DateTime.Now});
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            _service.RemoveAllLocations();
        }
    }
}
